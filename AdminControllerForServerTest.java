package ro.utcluj.Controller;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ro.utcluj.DataAccess.CookieDAO;
import ro.utcluj.DataAccess.OrderDAO;
import ro.utcluj.DataAccess.RegularUserDAO;
import ro.utcluj.Model.Cookie;
import ro.utcluj.Model.RegularUser;
import ro.utcluj.Service.CookieManager;
import ro.utcluj.Service.OrderManager;
import ro.utcluj.Service.RegularUserManager;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class AdminControllerForServerTest {
    @Mock
    private CookieDAO cookieDAO;
    @Mock
    private CookieManager cookieManager;
    @Mock
    private RegularUserDAO regularUserDAO;
    @Mock
    private RegularUserManager regularUserManager;
    @Mock
    private OrderDAO orderDAO;
    @Mock
    private OrderManager orderManager;

    private SessionFactory sessionFactory;

    @InjectMocks
    private AdminControllerForServer adminControllerForServer;

    @Before
    public void before() {
        sessionFactory = createSessionFactory();
        MockitoAnnotations.initMocks(this);

        regularUserDAO.setSessionFactory(sessionFactory);
        regularUserManager.setRegularUserDAO(regularUserDAO);

        adminControllerForServer.setRegularUserDAO(regularUserDAO);
        adminControllerForServer.setRegularUserManager(regularUserManager);

        cookieDAO.setSessionFactory(sessionFactory);
        cookieManager.setCookieDAO(cookieDAO);

        adminControllerForServer.setCookieDAO(cookieDAO);
        adminControllerForServer.setCookieManager(cookieManager);

        orderDAO.setSessionFactory(sessionFactory);
        orderManager.setOrderDAO(orderDAO);

        adminControllerForServer.setOrderDAO(orderDAO);
        adminControllerForServer.setOrderManager(orderManager);
    }

    private SessionFactory createSessionFactory() {
        StandardServiceRegistryBuilder standardServiceRegistryBuilder = new StandardServiceRegistryBuilder();
        StandardServiceRegistry standardServiceRegistry = standardServiceRegistryBuilder
                .configure()
                .build();
        MetadataSources metadataSources = new MetadataSources(standardServiceRegistry);
        Metadata metadata = metadataSources.buildMetadata();
        SessionFactory sessionFactory = metadata.buildSessionFactory();
        return sessionFactory;
    }

    @Test
    public void createCookie() {
        String expected = "Cookie creat cu succes!";
        Cookie cookie = new Cookie("Oreo", 5, 15, 2);
        when(cookieManager.createCookie(cookie)).thenReturn(true);

        String result = adminControllerForServer.createCookie(cookie);
        assertEquals(expected, result);
    }

    @Test
    public void deleteCookie() {
        String expected = "Cookie sters cu succes!";
        Cookie cookie = new Cookie("Oreo", 5, 15, 2);
        when(cookieManager.deleteCookie(cookie)).thenReturn(true);

        String result = adminControllerForServer.deleteCookie(cookie);
        assertEquals(expected, result);
    }

    @Test
    public void updateCookie() {
        String expected = "Succes update!";
        Cookie cookie = new Cookie("Oreo", 5, 15, 2);
        when(cookieManager.updateCookie(cookie)).thenReturn(true);

        String result = adminControllerForServer.updateCookie(cookie);
        assertEquals(expected, result);
    }

    @Test
    public void searchCookie() {
        List<Cookie> list = new ArrayList<>();
        Cookie cookie = new Cookie("Oreo", 5, 15, 2);
        list.add(cookie);
        when(cookieManager.viewAllCookies()).thenReturn(list);

        String result = adminControllerForServer.searchCookie();

        assertEquals(list.toString(), result);
    }

    @Test
    public void createRegularUser() {
        String expected = "User creat cu succes!";
        RegularUser regularUser = new RegularUser();
        when(regularUserManager.createUser(regularUser)).thenReturn(true);

        String result = adminControllerForServer.createRegularUser(regularUser);
        assertEquals(expected, result);
    }

    @Test
    public void deleteRegularUser() {
        String expected = "Success delete!";
        RegularUser regularUser = new RegularUser();
        when(regularUserManager.deleteUser(regularUser)).thenReturn(true);

        String result = adminControllerForServer.deleteRegularUser(regularUser);
        assertEquals(expected, result);
    }

    @Test
    public void updateRegularUser() {
        String expected = "Success update";
        RegularUser regularUser = new RegularUser();
        when(regularUserManager.updateUser(regularUser)).thenReturn(true);

        String result = adminControllerForServer.updateRegularUser(regularUser);
        assertEquals(expected, result);
    }

}