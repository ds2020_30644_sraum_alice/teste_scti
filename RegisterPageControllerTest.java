package ro.utcluj.Controller;

import org.junit.jupiter.api.Test;
import ro.utcluj.View.IRegisterView;
import ro.utcluj.View.IRegularUserView;
import ro.utcluj.View.impl.RegularUserView;

import javax.swing.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class RegisterPageControllerTest {

    @Test
    void createRegularUser() {
        IRegisterView registerView = mock(IRegisterView.class);
        when(registerView.getUsernameField()).thenReturn("test");
        when(registerView.getPasswordField()).thenReturn("test");
        when(registerView.getAddressField()).thenReturn("TEST.TEST");
        when(registerView.getAmountField()).thenReturn(300.5);
        when(registerView.getCNPField()).thenReturn("2990867574412");
        when(registerView.getTelNumberField()).thenReturn("0758907655");
        when(registerView.getCardNumberField()).thenReturn("4567890290785");

        RegisterPageController registerPageController = new RegisterPageController(registerView);
        registerPageController.createRegularUser();
        //se afiseaza un mesaj de informare
    }
}