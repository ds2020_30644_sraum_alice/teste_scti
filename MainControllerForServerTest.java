package ro.utcluj.Controller;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ro.utcluj.DataAccess.RegularUserDAO;
import ro.utcluj.Service.RegularUserManager;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class MainControllerForServerTest {

    private SessionFactory sessionFactory;

    @Mock
    private RegularUserDAO regularUserDAO;

    @Mock
    private RegularUserManager regularUserManager;

    @InjectMocks
    private MainControllerForServer mainControllerForServer;

    @Before
    public void before() {
        sessionFactory = createSessionFactory();
        MockitoAnnotations.initMocks(this);
        regularUserDAO.setSessionFactory(sessionFactory);
        regularUserManager.setRegularUserDAO(regularUserDAO);
        MainControllerForServer.setRegularUserDAO(regularUserDAO);
        MainControllerForServer.setRegularUserManager(regularUserManager);
    }

    private SessionFactory createSessionFactory() {
        StandardServiceRegistryBuilder standardServiceRegistryBuilder = new StandardServiceRegistryBuilder();
        StandardServiceRegistry standardServiceRegistry = standardServiceRegistryBuilder
                .configure()
                .build();
        MetadataSources metadataSources = new MetadataSources(standardServiceRegistry);
        Metadata metadata = metadataSources.buildMetadata();
        SessionFactory sessionFactory = metadata.buildSessionFactory();
        return sessionFactory;
    }


    @Test
    public void login() {
        when(regularUserManager.searchByUsernameAndPassword("ps","ps")).thenReturn(true);

        boolean result = mainControllerForServer.login("ps", "ps");
        assertEquals(true, result);
    }
}