package ro.utcluj.Controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.testng.Assert.assertEquals;

@RunWith(Parameterized.class)
public class AdminControllerForServerParametrizedTest {

    @Parameterized.Parameter(value = 0)
    public String expectedCookieType;

    @Parameterized.Parameter(value = 1)
    public double cookiePrice;



    @Test
    public void verifyCookieType() {
        assertEquals(expectedCookieType, new PremiumRulesEngine().getPremiumFactor(cookiePrice, expectedCookieType));
    }

}
