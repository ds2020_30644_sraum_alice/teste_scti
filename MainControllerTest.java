package ro.utcluj.Controller;

import org.junit.jupiter.api.Test;
import ro.utcluj.View.IMainView;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class MainControllerTest {

    @Test
    void loginAsAdmin() throws IOException {
        IMainView mainView = mock(IMainView.class);
        when(mainView.getCode()).thenReturn(java.util.Optional.of("1234"));

        MainController mainController = new MainController(mainView);
        mainController.loginAsAdmin();

        verify(mainView).showAdminPage();
    }

}