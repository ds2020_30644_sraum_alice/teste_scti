package ro.utcluj.Controller;

import org.junit.jupiter.api.Test;
import ro.utcluj.View.IAdminView;

import java.io.IOException;

import static org.mockito.Mockito.*;


public class AdminControllerTest {

    @Test
    public void createCookie() throws IOException {
        IAdminView adminView = mock(IAdminView.class);
        when(adminView.getCookiePrice()).thenReturn(5.5);
        when(adminView.getCookieType()).thenReturn("Papanasi");
        when(adminView.getCookieQOS()).thenReturn(30.2);

        AdminController adminController = new AdminController(adminView);
        adminController.createCookie();

        adminView.cookieStageInit();
        adminView.showCookiePage();
        verify(adminView).cookieStageInit();
        verify(adminView).showCookiePage();
    }

    @Test
    public void deleteCookie() throws IOException {
        IAdminView adminView = mock(IAdminView.class);
        when(adminView.getCookiePrice()).thenReturn(5.5);
        when(adminView.getCookieType()).thenReturn("Papanasi");
        when(adminView.getCookieQOS()).thenReturn(30.2);

        AdminController adminController = new AdminController(adminView);
        adminController.deleteCookie();

        adminView.cookieStageInit();
        adminView.showCookiePage();
        verify(adminView).cookieStageInit();
        verify(adminView).showCookiePage();
    }

    @Test
    public void updateCookie() throws IOException {
        IAdminView adminView = mock(IAdminView.class);
        when(adminView.getCookiePrice()).thenReturn(5.5);
        when(adminView.getCookieType()).thenReturn("Papanasi");
        when(adminView.getCookieQOS()).thenReturn(30.2);

        AdminController adminController = new AdminController(adminView);
        adminController.updateCookie();

        adminView.cookieStageInit();
        adminView.showCookiePage();
        verify(adminView).cookieStageInit();
        verify(adminView).showCookiePage();
    }

    @Test
    public void searchCookie() throws IOException {
        IAdminView adminView = mock(IAdminView.class);
        when(adminView.getCookiePrice()).thenReturn(5.5);
        when(adminView.getCookieType()).thenReturn("Papanasi");
        when(adminView.getCookieQOS()).thenReturn(30.2);

        AdminController adminController = new AdminController(adminView);
        adminController.searchCookie();

        adminView.cookieStageInit();
        adminView.showCookiePage();
        verify(adminView).cookieStageInit();
        verify(adminView).showCookiePage();
    }

    @Test
    void createRegularUser() {
        IAdminView adminView = mock(IAdminView.class);
        when(adminView.getUsernameField()).thenReturn("alice.sraum2");
        when(adminView.getPasswordField()).thenReturn("testPS");
        when(adminView.getCNPField()).thenReturn("2990387650027");
        when(adminView.getTelNumberField()).thenReturn("075978654490");

        AdminController adminController = new AdminController(adminView);
        adminController.createRegularUser();
    }

    @Test
    void deleteRegularUser() {
        IAdminView adminView = mock(IAdminView.class);
        when(adminView.getUsernameField()).thenReturn("alice.sraum2");
        when(adminView.getPasswordField()).thenReturn("testPS");
        when(adminView.getCNPField()).thenReturn("2990387650027");
        when(adminView.getTelNumberField()).thenReturn("075978654490");


        AdminController adminController = new AdminController(adminView);
        adminController.deleteRegularUser();
    }

    @Test
    void updateRegularUser() {
        IAdminView adminView = mock(IAdminView.class);
        when(adminView.getUsernameField()).thenReturn("ale.covaci");
        when(adminView.getPasswordField()).thenReturn("Bailarte");
        when(adminView.getCNPField()).thenReturn("2990324280014");
        when(adminView.getTelNumberField()).thenReturn("");

        AdminController adminController = new AdminController(adminView);
        adminController.updateRegularUser();
    }

    @Test
    void searchRegularUser() {
        IAdminView adminView = mock(IAdminView.class);
        when(adminView.getUsernameField()).thenReturn("alice.sraum");

        AdminController adminController = new AdminController(adminView);
        adminController.searchRegularUser();
    }

}