package ro.utcluj.Controller;

import org.junit.jupiter.api.Test;
import ro.utcluj.View.IRegularUserView;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class RegularUserPageControllerTest {

    @Test
    void searchByType() {
        IRegularUserView regularUserView = mock(IRegularUserView.class);
        when(regularUserView.getValue()).thenReturn("Papanasi");
        when(regularUserView.getSearchComboBox()).thenReturn("Type");

        RegularUserPageController regularUserPageController = new RegularUserPageController(regularUserView);
        regularUserPageController.searchBySmt();
    }

    @Test
    void searchByPrice() {
        IRegularUserView regularUserView = mock(IRegularUserView.class);
        when(regularUserView.getValue()).thenReturn("7");
        when(regularUserView.getSearchComboBox()).thenReturn("Price");

        RegularUserPageController regularUserPageController = new RegularUserPageController(regularUserView);
        regularUserPageController.searchBySmt();
    }

    @Test
    void searchByQOS() {
        IRegularUserView regularUserView = mock(IRegularUserView.class);
        when(regularUserView.getValue()).thenReturn("30.2");
        when(regularUserView.getSearchComboBox()).thenReturn("Quantity Of Sweeteners");

        RegularUserPageController regularUserPageController = new RegularUserPageController(regularUserView);
        regularUserPageController.searchBySmt();
    }

    @Test
    void buying() {
        IRegularUserView regularUserView = mock(IRegularUserView.class);
        when(regularUserView.getCookieType()).thenReturn("Papanasi");
        when(regularUserView.getCookieQuantity()).thenReturn("1");

        RegularUserPageController regularUserPageController = new RegularUserPageController(regularUserView);
        regularUserPageController.buying(java.util.Optional.of("alice.sraum"));
    }
}